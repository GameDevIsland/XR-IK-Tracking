﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VRMap
{
    public Transform VRTarget;
    public Transform rigTarget;
    public Vector3 trackingPositionOffset;
    public Vector3 trackingRotationOffset;

    public void Map()
    {
        rigTarget.position = VRTarget.TransformPoint(trackingPositionOffset);
        rigTarget.rotation = VRTarget.rotation * Quaternion.Euler(trackingRotationOffset);
    }
}

public class VRRig : MonoBehaviour
{
    public VRMap Head;
    public VRMap RightHand;
    public VRMap LeftHand;

    public Transform headConstraint;
    public Vector3 headBodyOffset;

    public float turnSmoothness;

    private void Start()
    {
        headBodyOffset = transform.position - headConstraint.position;
        Debug.Log(headBodyOffset);
    }

    private void LateUpdate()
    {
        transform.position = headConstraint.position + headBodyOffset;
        transform.forward = Vector3.Lerp (transform.forward,
            Vector3.ProjectOnPlane(headConstraint.up, Vector3.up).normalized,
            Time.deltaTime * turnSmoothness);

        Head.Map();
        RightHand.Map();
        LeftHand.Map();
    }
}
